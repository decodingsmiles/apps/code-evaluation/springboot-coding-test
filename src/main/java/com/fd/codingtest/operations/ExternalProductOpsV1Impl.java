package com.fd.codingtest.operations;

import org.springframework.stereotype.Service;

@Service("vendor1")
public class ExternalProductOpsV1Impl implements ExternalProductOps {
	
	private static final String CODE_FORMAT = "%s-12X";
	
	

	@Override
	public String getExternalVendorProductCode(String vendorCode) {
		
		return String.format(CODE_FORMAT, vendorCode);
	}

}
