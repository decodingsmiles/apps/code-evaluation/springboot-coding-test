package com.fd.codingtest.operations;

import org.springframework.stereotype.Service;

@Service("vendor2")
public class ExternalProductOpsV2Impl implements ExternalProductOps {

	private static final String CODE_FORMAT = "%s-1120RD";

	@Override
	public String getExternalVendorProductCode(String vendorCode) {
		return String.format(CODE_FORMAT, vendorCode);
	}

}
