package com.fd.codingtest.operations;

public interface ExternalProductOps {
	
	
	public String getExternalVendorProductCode(String vendorCode);

}
